package org.example.listeners;

import org.example.domain.BaseEntity;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

public class BaseEntityListener {

    @PrePersist
    public void beforeBaseEntityCreated(BaseEntity user) {
        user.setDateCreated(LocalDateTime.now());
        user.setDateUpdated(LocalDateTime.now());
    }

    @PreUpdate
    public void beforeBaseEntityUpdated(BaseEntity user) {
        user.setDateUpdated(LocalDateTime.now());
    }
}
