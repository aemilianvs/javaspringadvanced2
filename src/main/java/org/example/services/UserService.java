package org.example.services;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.example.domain.User;
import org.example.domain.UserDetails;
import org.example.repositories.UserDetailsRepository;
import org.example.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class UserService {

    private static Cache<UUID, User> usersCache = CacheBuilder.newBuilder()
            .initialCapacity(10).expireAfterAccess(10, TimeUnit.HOURS)
            .build();

    private UserRepository userRepository;
    private UserDetailsRepository userDetailsRepository;

    public UserService(UserRepository userRepository, UserDetailsRepository userDetailsRepository) {
        this.userRepository = userRepository;
        this.userDetailsRepository = userDetailsRepository;
        userRepository.findAll().forEach(user -> usersCache.put(user.getId(), user));
    }

    public List<User> getUsers() {
        final List<User> users = new ArrayList<>();
        usersCache.asMap().values().forEach(users::add);
        return users;
    }

    public User getUserById(UUID id) {
        User user = usersCache.getIfPresent(id);
        if (user == null) {
            user = userRepository.findById(id).orElse(null);
            if (user != null) {
                usersCache.put(user.getId(), user);
            }
        }
        return user;
    }

    public User saveUser(User user) {
        User result = this.userRepository.save(user);
        try {
            usersCache.put(result.getId(), result);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public UserDetails getUserDetails(User user) {
        return this.userDetailsRepository.findFirstByUser(user);
    }
}
