package org.example.repositories;

import org.example.domain.User;
import org.example.domain.UserDetails;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.UUID;

@Repository
@Transactional
public interface UserDetailsRepository extends CrudRepository<UserDetails, UUID> {
    UserDetails findFirstByUser(User user);
}
