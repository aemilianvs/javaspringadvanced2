package org.example.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.listeners.BaseEntityListener;
import org.example.model.Status;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Entity(name = "users")
@Table(
        uniqueConstraints = @UniqueConstraint(
                columnNames = {"display_name", "email"},
                name = "uniqueUsernameEmail"
        ),
        indexes = @Index(columnList = "status", name = "indexStatus")
)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "display_name")
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false, length = 50)
    private String email;

    @Enumerated
    private Status status;

    @OneToOne(cascade = CascadeType.ALL)
    private UserDetails userDetails;
}
